package net.skybert.cheese;

public class Person {
  private String name;

  public Person(final String pName) {
    name = pName;
  }

  @Override
  public String toString() {
    return "Person [name=" + name + "]";
  }
}
