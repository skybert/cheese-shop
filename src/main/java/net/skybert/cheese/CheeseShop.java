package net.skybert.cheese;

import java.util.ArrayList;
import java.util.List;

/**
 * CheeseShop
 */
public class CheeseShop {
  List<Person> persons = new ArrayList<>();

  public void enter(final Person pPerson) {
    persons.add(pPerson);
  }

  public List<Person> getPersons() {
    return persons;
  }

}