package net.skybert.cheese;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * CheeseShopTest
 */
public class CheeseShopTest {
  @Test
  public void personCanEnterCheeseShop() {
    CheeseShop shop = new CheeseShop();
    Person person = new Person("John");
    shop.enter(person);
    Assertions.assertEquals(1, shop.getPersons().size());
  }
}
